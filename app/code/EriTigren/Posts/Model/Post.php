<?php
namespace EriTigren\Posts\Model;
use Magento\Framework\Model\AbstractModel;

class Post extends AbstractModel
{
    const POST_ID = 'post_id';

    protected $_eventPrefix = 'posts';

    protected $_eventObject = 'post';

    protected $_idFieldName = self::POST_ID;

	protected function _construct()
	{
		$this->_init('EriTigren\Posts\Model\ResourceModel\Post');
	}
}
