<?php
namespace EriTigren\Posts\Model\ResourceModel\Post;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = \EriTigren\Posts\Model\Post::POST_ID;
	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('EriTigren\Posts\Model\Post', 'EriTigren\Posts\Model\ResourceModel\Post');
	}

}
