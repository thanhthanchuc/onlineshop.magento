<?php
namespace EriTigren\Posts\Model\ResourceModel;


class Post extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	protected function _construct()
	{
		$this->_init('tigren_posts', 'post_id');
	}
	
}