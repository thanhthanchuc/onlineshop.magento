<?php
namespace EriTigren\Posts\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Json\DecoderInterface;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var EncoderInterface
     */
    private $encoder;

    /**
     * @var DecoderInterface
     */
    private $decoder;

    public function __construct(
        EncoderInterface $encoder,
        DecoderInterface $decoder
    ) {
        $this->encoder = $encoder;
        $this->decoder = $decoder;
    }

    private function upgradeTo112(SchemaSetupInterface $setup)
    {
        $tableName = $setup->getTable('tigren_posts');
        $table = $setup->getConnection()
            ->newTable($tableName)
            ->addColumn(
                'post_id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Trusted post ID'
            )
            ->addColumn(
                'title',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Title of a post'
            )
            ->addColumn(
                'url_key',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Url for this post'
            )
            ->addColumn(
                'content',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Content of a post'
            )
            ->addColumn(
                'tags',
                Table::TYPE_TEXT,
                64,
                ['nullable' => false],
                'Tag of a post'
            )
            ->addColumn(
                'status',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'status of a post'
            )
            ->addColumn(
                'featured_image',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'image of a post'
            )
            ->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'status of a post'
            )
            ->addColumn(
                'updated_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                'status of a post'
            );

        $setup->getConnection()->createTable($table);
    }

    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.1.2') < 0) {
            $this->upgradeTo112($setup);
        }

        $setup->endSetup();
    }
}
