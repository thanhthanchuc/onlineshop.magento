<?php
namespace Tigren\Jobs\Model\ResourceModel\Department;
 
use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
 
class Collection extends AbstractCollection
{
 
    protected $_idFieldName = \Tigren\Jobs\Model\Department::DEPARTMENT_ID;
     
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tigren\Jobs\Model\Department', 'Tigren\Jobs\Model\ResourceModel\Department');
    }
 
}